<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
class Pcode extends Eloquent
{
	protected $collection = 'pcodes';

    protected $fillable = ['pcode'];
}
