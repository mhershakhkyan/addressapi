<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pcode;
use App\Addres;
use GuzzleHttp\Client;
use Carbon\Carbon;
class PcodeController extends Controller
{

	public function getForm() {

		$datas = Addres::all();
		return view('welcome', compact('datas'));
	}


    public function checkPostalCode(Request	$request)
    {
    	$pcode = Addres::where('pcode', $request->pcode)->first();
    	$client = new Client();

    	if($pcode) {
			$now = Carbon::now();
			$end = Carbon::parse($pcode->updated_at);
			 $diff = $end->diffInDays($now);
			
    		if($diff <= 21) {
				return $pcode;
    		} else {
    			$code = $request->pcode;
	    		if($code) {
		    		$key = 'dIgtloaPhku-d8Y_iquMlw14873';
		    		$uri = 'https://api.getaddress.io/find/'.$code.'?api-key='.$key;
		    		$res = $client->request('GET', $uri);
		    		$status = $res->getStatusCode();
		    		if($status == 200) {
		    			$address = json_decode($res->getBody());

		    			$pcode->address = $address;
		    			if($pcode->update()) {
		    				return $pcode;
		    			}
		    		} else {
		    			return response()->json('not found');
		    		}
		    	}	
    		}
    	} else {
    		$code = $request->pcode;
    		if($code) {
	    		$key = 'dIgtloaPhku-d8Y_iquMlw14873';
	    		$uri = 'https://api.getaddress.io/find/'.$code.'?api-key='.$key;
	    		$res = $client->request('GET', $uri);
	    		$status = $res->getStatusCode();
	    		if($status == 200) {
	    			$address = json_decode($res->getBody());

	    			$adress = new Addres();
	    			$adress->pcode = $code;
	    			$adress->address = $address;
	    			if($adress->save()) {
	    				return $adress;
	    			}
	    		} else {
	    			return response()->json('not found');
	    		}
	    		
	    	}
    	}

    }
}
